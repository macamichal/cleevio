package com.example.cleevio.ui.favourite

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cleevio.model.Currency
import com.example.cleevio.model.Rate
import com.example.cleevio.repository.CurrencyRepository
import com.example.cleevio.repository.DbRepository
import com.example.cleevio.util.Result
import com.orm.SugarRecord
import kotlinx.coroutines.launch

class FavouriteViewModel : ViewModel() {

    private var TAG = FavouriteViewModel::class.java.simpleName

    private val dbRepository = DbRepository()
    private val currencyRepository = CurrencyRepository()
    var currency: Currency = Currency()


    private val currencyData = MutableLiveData<Result<Currency>>()
    val currencyState: LiveData<Result<Currency>> = currencyData

    fun loadData() {
        Log.d(TAG, "load data")
        currencyData.value = Result.Loading

        viewModelScope.launch {
            val resDb = dbRepository.loadCurrency()
            if (resDb is Result.Success) {
                currency = resDb.data
                currencyData.value = resDb
            }


            val resApi = currencyRepository.loadData()
            if (resApi is Result.Success) {
                currency = resApi.data
            }
            currencyData.value = resApi
        }
    }

    fun changeFavourite(rate: Rate) {
        val r: Rate? = SugarRecord.find(Rate::class.java, "name = ?", rate.name).getOrNull(0)

        if (r != null) {
            r.favourite = !r.favourite
            r.save()
            changeListValue(r)
        } else {
            rate.favourite = true
            rate.save()
            changeListValue(rate)
        }
    }

    private fun changeListValue(rate: Rate) {
        currency.rateList.find { it.name == rate.name }?.favourite = rate.favourite
        currencyData.value = Result.Success(currency)
    }

}
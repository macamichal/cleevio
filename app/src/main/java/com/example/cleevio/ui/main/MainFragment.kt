package com.example.cleevio.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.cleevio.MainActivity
import com.example.cleevio.R
import com.example.cleevio.model.Currency
import com.example.cleevio.ui.favourite.FavouriteFragment
import com.example.cleevio.util.Result
import com.example.cleevio.view.show
import kotlinx.android.synthetic.main.favourite_fragment.list
import kotlinx.android.synthetic.main.favourite_fragment.progress
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    private var TAG = MainFragment::class.java.simpleName
    private val favouriteAdapter = MainAdapter(arrayListOf())
    private lateinit var viewModel: MainViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        floating_action_button.setOnClickListener { openFavourite() }

        amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.computeAmount(if(s.toString().toDoubleOrNull() != null) s.toString().toDouble() else 0.0)
            }
        })

        list.apply {
            adapter = favouriteAdapter
        }

        subscribeUi()

        viewModel.loadData()
    }

    private fun subscribeUi() {
        viewModel.currencyState.observe(this, Observer<Result<Currency>> { result ->

            Log.d(TAG, "result: $result")

            no_favourites.show(false)

            when (result) {
                Result.Loading -> {
                    showProgress(true)
                }
                is Result.Success<Currency> -> {
                    showProgress(false)
                    Log.d(TAG, "data ${result.data}")
                    if (result.data != null && result.data.rateList.size > 0) {
                        favouriteAdapter.set = result.data.rateList
                    } else {
                        favouriteAdapter.set = mutableListOf()
                        no_favourites.show(true)
                    }
                    favouriteAdapter.notifyDataSetChanged()
                }
                is Result.Error -> {
                    showProgress(false)
                    no_favourites.show(true)
                    Log.d(TAG, "error: ${result.exception}")
                    Toast.makeText(context, result.exception.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun showProgress(value: Boolean) {
        list.show(!value)
        progress.show(value)
    }

    private fun openFavourite() {
        (activity as MainActivity).replaceFragment(FavouriteFragment())
    }

}
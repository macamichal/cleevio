package com.example.cleevio.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cleevio.R
import com.example.cleevio.model.Rate
import kotlinx.android.synthetic.main.item_list_main.view.*

class MainAdapter(var set: MutableList<Rate>) :
    RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
        MainViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_main, parent, false)
        )

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(set[position])
    }

    override fun getItemCount(): Int = set.size

    class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name = view.text_name
        private val currency = view.text_currency
        private val computed = view.text_computed

        fun bind(rate: Rate) {
            name.text = rate.name
            currency.text = rate.currency.toString()
            computed.text = rate.computed.toString()
        }
    }
}
package com.example.cleevio.ui.favourite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cleevio.R
import com.example.cleevio.model.Rate
import kotlinx.android.synthetic.main.item_list_favourite.view.*

class FavouriteAdapter(var dataSet: MutableList<Rate>, val onClick: (Rate) -> Unit) :
    RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder =
        FavouriteViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_favourite, parent, false)
        )

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        holder.bind(dataSet[position], onClick)
    }

    override fun getItemCount(): Int = dataSet.size

    class FavouriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name = view.text_name
        private val currency = view.text_currency
        private val favouriteBtn = view.favourite

        fun bind(rate: Rate, onClick: (Rate) -> Unit) {
            name.text = rate.name
            currency.text = rate.currency.toString()
            val resource =
                if (rate.favourite!!) R.drawable.ic_favorite_black_24dp else R.drawable.ic_favorite_border_black_24dp
            favouriteBtn.setImageResource(resource)
            favouriteBtn.setOnClickListener { onClick(rate) }
        }
    }
}
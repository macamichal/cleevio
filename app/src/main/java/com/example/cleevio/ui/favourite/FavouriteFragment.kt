package com.example.cleevio.ui.favourite

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.cleevio.MainActivity
import com.example.cleevio.R
import com.example.cleevio.model.Currency
import com.example.cleevio.model.Rate
import com.example.cleevio.util.Result
import com.example.cleevio.view.show
import kotlinx.android.synthetic.main.favourite_fragment.*

class FavouriteFragment : Fragment() {

    private var TAG = FavouriteFragment::class.java.simpleName
    private val favouriteAdapter = FavouriteAdapter(arrayListOf()) { doAction(it) }
    private lateinit var viewModel: FavouriteViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.favourite_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProviders.of(this).get(FavouriteViewModel::class.java)

        list.apply {
            adapter = favouriteAdapter
        }

        subscribeUi()

        if (savedInstanceState == null) {
            Log.d(TAG, "load data")
            viewModel.loadData()
        }
    }

    private fun subscribeUi() {
        viewModel.currencyState.observe(this, Observer<Result<Currency>> { result ->

            when (result) {
                Result.Loading -> {
                    showProgress(true)
                }
                is Result.Success<Currency> -> {
                    showProgress(false)
                    Log.d(TAG, "data size ${result.data.rateList.size}")
                    favouriteAdapter.dataSet = result.data.rateList
                    favouriteAdapter.notifyDataSetChanged()
                }
                is Result.Error -> {
                    showProgress(false)
                    Log.d(TAG, "error: ${result.exception}")
                    Toast.makeText(context, result.exception.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun showProgress(value: Boolean) {
        list.show(!value)
        progress.show(value)
    }

    private fun doAction(item: Rate) {
        Log.d(TAG, "rate: $item")
        viewModel.changeFavourite(item)
    }

}

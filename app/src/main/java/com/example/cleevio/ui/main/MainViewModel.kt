package com.example.cleevio.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cleevio.model.Currency
import com.example.cleevio.repository.CurrencyRepository
import com.example.cleevio.repository.DbRepository
import com.example.cleevio.util.Result
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private var TAG = MainViewModel::class.java.simpleName
    private val dbRepository = DbRepository()
    private val currencyRepository = CurrencyRepository()

    var amount: Double = 0.0
    var currency: Currency = Currency()

    private val currencyData = MutableLiveData<Result<Currency>>()
    val currencyState: LiveData<Result<Currency>> = currencyData

    fun loadData() {
        Log.d(TAG, "load data")
        currencyData.value = Result.Loading

        loadFavourites()

        //actualize data
        viewModelScope.launch {
            val resApi = currencyRepository.loadData()
            if (resApi is Result.Success) {
                loadFavourites()
            }

        }
    }


    fun computeAmount(amount: Double) {
        this.amount = amount
        for (r in currency.rateList) r.computed = r.currency * this.amount
        currencyData.value = Result.Success(currency)
    }

    private fun loadFavourites() {
        viewModelScope.launch {
            val res = dbRepository.loadFavourites()
            if (res is Result.Success) {
                currency = res.data
                computeAmount(amount)
            }
        }
    }

}

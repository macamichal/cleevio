package com.example.cleevio.repository

import com.example.cleevio.model.Currency
import com.example.cleevio.model.Rate
import com.example.cleevio.repository.retrofit.Retrofit
import com.example.cleevio.repository.retrofit.api.CurrencyApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.io.IOException
import java.lang.Exception
import com.example.cleevio.util.Result
import com.orm.SugarRecord

class CurrencyRepository {

    private val storyApi: CurrencyApi =
        Retrofit().creteClient(CurrencyApi::class.java, "http://data.fixer.io")

    suspend fun loadData(): Result<Currency> {
        return withContext(Dispatchers.IO) {
            val response: Response<Currency>

            try {
                response = storyApi.getCurrencies().execute()
            } catch (exception: IOException) {
                return@withContext Result.Error(exception)
            }

            return@withContext if (response.isSuccessful) {
                val currency = response.body()
                currency.rateList = mutableListOf<Rate>()

                //get all favourites
                val favourites: List<Rate> =
                    SugarRecord.find(Rate::class.java, "favourite = ?", 1.toString())

                //set favourites
                currency.rates?.forEach {
                    var rate = Rate(it.key, it.value)
                    if (favourites.find { it.name == rate.name } != null) {
                        rate.favourite = true
                    }
                    currency.rateList.add(rate)
                    //save rate
                    rate.save()
                }

                currency.rateList.sortBy { it.name }

                //save only one currency object
                currency.id = 1
                currency.save()

                Result.Success(currency)
            } else {
                Result.Error(Exception("${response.code()}: ${response.message()}"))
            }
        }
    }

}
package com.example.cleevio.repository.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Retrofit {
    fun <T> creteClient(clazz: Class<T>, baseUrl: String): T = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .client((OkHttpClient().newBuilder().build()))
        .build().create(clazz)
}
package com.example.cleevio.repository.retrofit.api

import com.example.cleevio.model.Currency
import retrofit2.Call
import retrofit2.http.GET

interface CurrencyApi {
    @GET("/api/latest?access_key=51bf7839760177b1840fb9f852574155")
    fun getCurrencies(): Call<Currency>
}
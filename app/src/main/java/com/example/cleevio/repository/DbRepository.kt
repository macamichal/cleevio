package com.example.cleevio.repository

import com.example.cleevio.model.Currency
import com.example.cleevio.model.Rate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.example.cleevio.util.Result
import com.orm.SugarRecord
import java.lang.Exception

class DbRepository {

    suspend fun loadFavourites(): Result<Currency> {
        return withContext(Dispatchers.IO) {

            val currency = SugarRecord.findById(Currency::class.java, 1)
            if (currency != null) {

                val favourites: MutableList<Rate> =
                    SugarRecord.find(Rate::class.java, "favourite = ?", 1.toString())
                        .toMutableList()
                currency.rateList = favourites

                currency.rateList.sortBy { it.name }
                return@withContext Result.Success(currency)
            } else return@withContext Result.Error(Exception("No currency"))
        }
    }

    suspend fun loadCurrency(): Result<Currency> {
        return withContext(Dispatchers.IO) {

            val currency = SugarRecord.findById(Currency::class.java, 1)
            if (currency != null) {
                val favourites: MutableList<Rate> =
                    SugarRecord.findAll(Rate::class.java).asSequence()
                        .toMutableList()
                favourites.sortBy { it.name }
                currency.rateList = favourites
                return@withContext Result.Success(currency)
            } else return@withContext Result.Error(Exception("No currency"))
        }
    }

}
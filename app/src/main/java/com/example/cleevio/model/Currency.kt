package com.example.cleevio.model

import android.os.Parcelable
import com.orm.SugarRecord
import com.orm.dsl.Ignore
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Currency(
    val base: String = "",
    val date: String = "",
    val timestamp: Long = 0,
    @Ignore val rates: HashMap<String, Double> = HashMap(),
    var rateList: MutableList<Rate> = mutableListOf()
) : Parcelable, SugarRecord()
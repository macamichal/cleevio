package com.example.cleevio.model

import android.os.Parcelable
import com.orm.SugarRecord
import com.orm.dsl.Ignore
import com.orm.dsl.Unique
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rate(
    @Unique var name: String = "",
    var currency: Double = 0.0,
    var favourite: Boolean = false,
    @Ignore var computed: Double = 0.0
) : Parcelable, SugarRecord()
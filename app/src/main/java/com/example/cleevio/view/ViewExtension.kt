package com.example.cleevio.view

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

fun ProgressBar.show(value: Boolean) {
    visibility = if (value) View.VISIBLE else View.GONE
}

fun RecyclerView.show(value: Boolean) {
    visibility = if (value) View.VISIBLE else View.GONE
}

fun TextView.show(value: Boolean) {
    visibility = if (value) View.VISIBLE else View.GONE
}